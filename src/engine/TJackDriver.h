/*
    Copyright (C) 2005-2024 Remon Sijrier
 
    This file is part of Traverso
 
    Traverso is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#ifndef JACKDRIVER_H
#define JACKDRIVER_H

#include "TAudioDriver.h"
#include "defines.h"
#include <jack/jack.h>
#include <QObject>
#include <QVector>

class TJackDriver : public TAudioDriver
{
    Q_OBJECT
public:
    TJackDriver(TAudioDevice* device);
    ~TJackDriver();

    int  process_callback (nframes_t nframes);
    int _read(nframes_t nframes);
    int _write(nframes_t nframes);
    int _run_cycle() {return 1;}
    int setup(QList<AudioChannel* > channels, const QString &projectName);
    int attach();
    int start();
    int stop();

    QString get_device_name();
    QString get_device_longname();

    void add_channel(AudioChannel* channel);
    void remove_channel(AudioChannel* channel);

    float get_cpu_load();

    size_t is_running() const {return m_running == 1;}
    jack_client_t* get_client() const {return m_jackClient;}
    bool is_slave() const {return m_isSlave;}
    void update_config();

    virtual bool supports_free_wheeling() const {
        return true;
    }

    void start_free_wheeling() final;
    void stop_free_wheeling() final;

    virtual bool runs_in_blocking_mode() const {
        return false;
    }


private:
    struct PortChannelPair {
        PortChannelPair() {
            jackport = nullptr;
            channel = nullptr;
        }

        jack_port_t*    jackport;
        AudioChannel*   channel;
        QString         name;
    };

    volatile size_t         m_running;
    jack_client_t*          m_jackClient;
    QList<PortChannelPair*> m_inputs;
    QList<PortChannelPair*> m_outputs;
    TTransportControl       m_transportControl;

    bool                    m_isSlave;

    int  jack_sync_callback (jack_transport_state_t, jack_position_t*);

    static int _xrun_callback(void *arg);
    static int  _process_callback (nframes_t nframes, void *arg);
    static int _bufsize_callback(jack_nframes_t nframes, void *arg);
    static void _freewheel_callback(int starting, void* arg);
    static void _on_jack_shutdown_callback(void* arg);
    static int  _jack_sync_callback (jack_transport_state_t, jack_position_t*, void *arg);

private slots:
    void private_add_port_channel_pair(PortChannelPair* pair);
    void private_remove_port_channel_pair(PortChannelPair* pair);
    void cleanup_removed_port_channel_pair(PortChannelPair* pair);

signals:
    void jackShutDown();
    void pcpairRemoved(PortChannelPair*);

};


#endif

//eof

