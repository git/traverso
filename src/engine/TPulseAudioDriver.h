/*
    Copyright (C) 2008 Remon Sijrier 
 
    This file is part of Traverso
 
    Traverso is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.
 
*/

#ifndef TPULSE_AUDIO_DRIVER_H
#define TPULSE_AUDIO_DRIVER_H

#include "TAudioBuffer.h"
#include "TAudioDriver.h"
#include "defines.h"
#include <pulse/pulseaudio.h>
#include <pulse/simple.h>

class TPulseAudioDriver : public TAudioDriver
{
    Q_OBJECT

public:
    TPulseAudioDriver(TAudioDevice* device);
	~TPulseAudioDriver();

	int _read(nframes_t nframes);
	int _write(nframes_t nframes);
	int _run_cycle();
	int setup(bool capture=true, bool playback=true, const QString& cardDevice="hw:0");
	int attach();
	int start();
	int stop();

	QString get_device_name();
	QString get_device_longname();

	void update_config();

    virtual bool is_realtime_capable() const {
        return false;
    }

private:
    // Simple PulseAudio
    pa_simple*  m_paSimplePlayback;
    pa_simple*  m_paSimpleCapture;
    pa_sample_spec m_sampleSpec{};
    TRealTimeAudioBuffer m_interleavedPlaybackBuffer;
    TRealTimeAudioBuffer m_interleavedCaptureBuffer;
};


#endif

//eof

 
