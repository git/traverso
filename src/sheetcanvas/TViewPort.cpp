/*
Copyright (C) 2005-2006 Remon Sijrier 

This file is part of Traverso

Traverso is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.

*/

#include <QMouseEvent>
#include <QResizeEvent>
#include <QEvent>
#include <QRect>
#include <QPixmap>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QEvent>
#include <QStyleOptionGraphicsItem>
#include <QApplication>

#include <Utils.h>
#include "TInputEventDispatcher.h"

#include "TSheetView.h"
#include "TClipsViewPort.h"
#include "TViewItem.h"
#include "TContextPointer.h"

#include "TInformUser.h"

#include "Debugger.h"


/**
 * \class TViewPort
 * \brief An Interface class to create Contextual, or so called 'Soft Selection' enabled Widgets.

	The TViewPort class inherits QGraphicsView, and thus is a true Canvas type of Widget.<br />
	Reimplement TViewPort to create a 'Soft Selection' enabled widget. You have to create <br />
	a QGraphicsScene object yourself, and set it as the scene the TViewPort visualizes.

	TViewPort should be used to visualize 'core' data objects. This is done by creating a <br />
	ViewItem object for each core class that has to be visualized. The naming convention <br />
	for classes that inherit ViewItem is: core class name + View.<br />
	E.g. the ViewItem class that represents an AudioClip should be named AudioClipView.

    All keyboard and mouse events by default are propagated to the InputEventDispatcher, which in <br />
    turn will parse the events. In case the event sequence was recognized by the InputEventDispatcher <br />
    it will ask a list of (pointed) ContextItem's from ContextPointer


 *	\sa ContextPointer, TInputEventDispatcher
 */

TViewPort::TViewPort(QGraphicsScene* scene, QWidget* parent)
    : QGraphicsView(scene, parent)
    , m_sv(nullptr)
{
    PENTERCONS;
    setFrameStyle(QFrame::NoFrame);
    setAlignment(Qt::AlignLeft | Qt::AlignTop);

    // be aware that if we use antialiazing we have to
    // paint within 2 pixels of the viewitem boudary else
    // make the bouding rect of that viewitem 2 pixels larger
    setOptimizationFlag(DontAdjustForAntialiasing);

    // each viewitem has to call save/store on painter themselves
    setOptimizationFlag(DontSavePainterState);

    // we do not rely on graphicsitem knowing if mouse hovers over it
    // so disable tracking the mouse over items.
    setInteractive(false);

    // but we do enable mouse enter/leave/move events of course so we
    // can inform cpointer and tinputeventdispatcher aoubt soft selected items
    setMouseTracking(true);

    // Although rare it used to be possible to leave the viewport with the mouse
    // without the viewport knowing, which meant, the mouse was grabbed for all eternity
    // which is very bad: no more interaction possible with other Widgets.
    // To remedy this situation, let's always release the mouse after 30 seconds, most
    // hold actions will keep working just fine anyways,and the few that don't, well, it is what it is
    m_mouseGrabCheckTime = 30000; // 30 seconds
    connect(&m_grabMouseGuardTimer, &QTimer::timeout, this, [this]() {
        if (QWidget::mouseGrabber() == this->viewport()) {
            tInformUser().information(tr("Mouse was grabbed for more then %1 seconds, releasing mouse now").arg(m_mouseGrabCheckTime / 1000));
            release_mouse();
        }
    });
}

TViewPort::~TViewPort()
{
	PENTERDES;

    if (cpointer().get_viewport() == this) {
        cpointer().set_current_viewport(nullptr);
    }
}

bool TViewPort::event(QEvent * event)
{
	// We want Tab events also send to the InputEngine
	// so treat them as 'normal' key events.
	if (event->type() == QEvent::KeyPress)
	{
		QKeyEvent *ke = static_cast<QKeyEvent *>(event);
		if (ke->key() == Qt::Key_Tab)
		{
			keyPressEvent(ke);
			return true;
		}
	}

	if (event->type() == QEvent::KeyRelease)
	{
		QKeyEvent *ke = static_cast<QKeyEvent *>(event);
		if (ke->key() == Qt::Key_Tab)
		{
			keyReleaseEvent(ke);
			return true;
		}
    }

	return QGraphicsView::event(event);
}

void TViewPort::grab_mouse()
{
    m_grabMouseGuardTimer.start(m_mouseGrabCheckTime);
   viewport()->grabMouse();
}

void TViewPort::release_mouse()
{
    m_grabMouseGuardTimer.stop();
    viewport()->releaseMouse();
}


void TViewPort::mouseMoveEvent(QMouseEvent* event)
{
    PENTER3;

    cpointer().update_mouse_positions(event->pos(), event->globalPosition());

    if (cpointer().keyboard_only_input()) {
        event->accept();
        return;
    }

    // Qt generates mouse move events when the scrollbars move
    // since a mouse move event generates a jog() call for the
    // active holding command, this has a number of nasty side effects :-(
    // For now, we ignore such events....
    if (event->pos() == m_previousMousePos) {
        event->accept();
        return;
    }

    m_previousMousePos = event->pos();

    if (ied().is_holding()) {
        // cpointer().update_mouse_positions() will instruct ied() to update holdcommand
        // of new mouse position, so nothing to be done here
        event->accept();
        return;
    }

    // here we detect which items are under the mouse cursor
    detect_items_below_cursor();

    event->accept();
}

void TViewPort::detect_items_below_cursor()
{
    QList<TViewItem*> mouseTrackingItems;

    QList<QGraphicsItem *> itemsUnderCursor = scene()->items(cpointer().scene_pos());
    QList<TContextItem*> activeContextItems;

    // since sheetview has no bounding rect, and should always have 'active context'
    // add it if it's available
    if (m_sv) {
        itemsUnderCursor.append(m_sv);
    }

    if (!itemsUnderCursor.isEmpty())
    {
        foreach(QGraphicsItem* item, itemsUnderCursor)
        {
            if (TViewItem::is_viewitem(item))
            {
                TViewItem* viewItem = static_cast<TViewItem*>(item);
                if (!viewItem->item_ignores_context())
                {
                    activeContextItems.append(viewItem);
                    if (viewItem->has_mouse_tracking())
                    {
                        mouseTrackingItems.append(viewItem);
                    }
                }
            }
        }
    } else {
        // If no item is below the mouse, default to default cursor
        set_canvas_cursor_shape(":/cursorFloat", Qt::AlignTop | Qt::AlignHCenter);
    }

    // printf("setting active context items for detect items below cursor %lld", activeContextItems.size());
    // update context pointer active context items list
    cpointer().set_active_context_items_by_mouse_movement(activeContextItems);

    // Some ViewItems want to track mouse move events themselves like CurveView
    // to update the soft selected node which cannot be done by the boudingRect of
    // CurveNode since it is too small.
    for(auto item : mouseTrackingItems) {
        item->mouse_hover_move_event();
    }
}

void TViewPort::tabletEvent(QTabletEvent * event)
{
    PMESG("ViewPort tablet event:: x, y: %d, %d", (int)event->position().x(), (int)event->position().y());
	PMESG("ViewPort tablet event:: high resolution x, y: %f, %f",
          event->globalPosition().x(), event->globalPosition().y());
//	cpointer().store_mouse_cursor_position((int)event->x(), (int)event->y());
	
	QGraphicsView::tabletEvent(event);
}

void TViewPort::enterEvent(QEnterEvent* e)
{
    if (ied().is_holding()) {
        // we allready have viewport so do nothing
        e->accept();
        return;
    }

    QGraphicsView::enterEvent(e);

    // even if the mouse is grabbed, the window system can set a default cursor on a leave event
    // with this we override that behavior
    // TODO: setOverrideCursor screws up when showing context menus :(
    // for now, default to old solution by only setting BlankCursor
//    QGuiApplication::setOverrideCursor(Qt::BlankCursor);
    if (m_sv) {
        // viewport()->setCursor(Qt::BlankCursor);
    }

	cpointer().set_current_viewport(this);
    setFocus();
    e->accept();
}

void TViewPort::leaveEvent(QEvent* e)
{
    if (ied().is_holding()) {
        e->accept();
        return;
    }

    // always restore an overrided cursor, we did set one in enterEvent()
//    QGuiApplication::restoreOverrideCursor();

    // FIXME
    // not having a viewport in contextpointer is fine when mouse cursor leaves the viewport
    // however, a leaveEvent is also created when a ContextMenu is shown
    // in special cases (Q qwerty browse Gain enter numerical input Q qwerty browse reject enter -> undefined state is entered)
    // When selecting with the mouse cursor an item from the menu there is no problem, probably due an enterEvent or at least
    // mouse move event is catched by ContextPointer
    // this can result in ContextPointer having an empty list of active context items
    // and a crash in TInputEventDispatcher finish_hold() when enter is pressed during this state
    // state is recovered when the mouse is moved
    cpointer().set_current_viewport(nullptr);

    // Force the next mouse move event to do something
    // even if the mouse didn't move, so switching viewports
    // does update the current context!
    m_previousMousePos = QPoint();
    e->accept();
}

void TViewPort::keyPressEvent( QKeyEvent * e)
{
	ied().catch_key_press(e);
	e->accept();
}

void TViewPort::keyReleaseEvent( QKeyEvent * e)
{
	ied().catch_key_release(e);
    e->accept();
}

void TViewPort::dragEnterEvent(QDragEnterEvent *event)
{
    event->accept();
}

void TViewPort::dragMoveEvent(QDragMoveEvent *event)
{
    event->ignore();
}

void TViewPort::mousePressEvent( QMouseEvent * e )
{
	ied().catch_mousebutton_press(e);
	e->accept();
}

void TViewPort::mouseReleaseEvent( QMouseEvent * e )
{
	ied().catch_mousebutton_release(e);
	e->accept();
}

void TViewPort::mouseDoubleClickEvent( QMouseEvent * e )
{
	ied().catch_mousebutton_press(e);
	e->accept();
}

void TViewPort::wheelEvent( QWheelEvent * e )
{
	ied().catch_scroll(e);
	e->accept();
}

void TViewPort::paintEvent( QPaintEvent* e )
{
// 	PWARN("ViewPort::paintEvent()");
	QGraphicsView::paintEvent(e);
}

void TViewPort::set_canvas_cursor_shape(const QString &shape, int alignment)
{
    if (m_sv) {
        m_sv->set_cursor_shape(shape, alignment);
    }
}

void TViewPort::set_canvas_cursor_text( const QString & text, int mseconds)
{
    if (!m_sv) {
        PERROR(QString("ViewPort::set_canvas_cursor_text: no sheetview set to set text %1").arg(text));
        return;
    }

	m_sv->set_edit_cursor_text(text, mseconds);
}

void TViewPort::set_canvas_cursor_pos(QPointF pos, CursorMoveReason reason)
{
    m_sv->set_canvas_cursor_pos(pos, reason);
}
