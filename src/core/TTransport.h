/*
Copyright (C) 2011 Remon Sijrier

This file is part of Traverso

Traverso is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.

*/

#ifndef TTRANSPORT_H
#define TTRANSPORT_H

#include "TCommand.h"
#include "TGlobalContext.h"

#include <QTimer>

class TCommand;

class TTransport : public TGlobalContext
{
	Q_OBJECT
public:

public slots:
    DISPATCH_RULE_IS_ALWAYS TCommand* start_transport();
    DISPATCH_RULE_IS_ALWAYS TCommand* set_recordable_and_start_transport();
    DISPATCH_RULE_IS_ALWAYS TCommand* to_start();
    DISPATCH_RULE_IS_ALWAYS TCommand* to_end();

    DISPATCH_RULE_IS_ALWAYS TCommand* next_skip_pos();
    DISPATCH_RULE_IS_ALWAYS TCommand* prev_skip_pos();

private:
    TTransport();
    TTransport(const TTransport&);

    QTimer              m_skipTimer;

    // allow this function to create one instance
    friend TTransport& transport();

};

TTransport& transport();

#endif // TTRANSPORT_H
