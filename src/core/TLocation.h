/*
Copyright (C) 2024 Remon Sijrier

This file is part of Traverso

Traverso is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.

*/

#ifndef T_LOCATION_H
#define T_LOCATION_H

#include "TTimeRef.h"
#include "qobject.h"

class TSnapList;


class TLocation : public QObject
{
    Q_OBJECT

public:
    TLocation(QObject* owner = nullptr);
    ~TLocation() {}

	void set_snappable(bool snap);

	bool is_snappable() const;

	void set_snap_list(TSnapList *sList);

    inline TTimeRef get_start() const {return m_start;}
    inline TTimeRef get_end() const {return m_end;}
    TTimeRef get_length() const {return m_end - m_start;}


    void set_location(QObject *owner, const TTimeRef& start, TTimeRef end = TTimeRef());
    void set_start(QObject *owner, const TTimeRef& start);
    void set_end(QObject *owner, const TTimeRef& end);

protected:
    TTimeRef     m_start;
    TTimeRef     m_end;

private:
    QObject*    m_owner;
	bool		m_isSnappable;
	TSnapList	*snapList;

signals:
    void locationChanged();
};


#endif

/* EOF */
