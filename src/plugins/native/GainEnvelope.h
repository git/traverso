/*
Copyright (C) 2007 Remon Sijrier

This file is part of Traverso

Traverso is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.

*/


#ifndef GAIN_ENVELOPE_H
#define GAIN_ENVELOPE_H

#include "TAudioPlugin.h"
#include "TTimeRef.h"

class TCurve;
class TSession;

class GainEnvelope : public TAudioPlugin
{
    Q_OBJECT

public:
    GainEnvelope(TSession* session);
    ~GainEnvelope(){}

    QDomNode get_state(QDomDocument doc);
    int set_state(const QDomNode & node );
    void process(AudioBus* bus, nframes_t nframes);
    void process_gain(AudioBus* audioBus, const TTimeRef& startlocation, const TTimeRef& endlocation, nframes_t nframes, uint channels);

    void set_session(TSession* session);
    void set_gain(float gain) {m_gain = gain;}

    float get_gain() const {return m_gain;}
    TCurve* get_curve();
    QString get_name();

public slots:
    QString get_gain_db_string(int decimals=1);


private:
    float m_gain;
};

#endif

